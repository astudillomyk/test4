package com.prueba4.test4.services;


import com.prueba4.test4.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;

@Service
public class ProductService {

    @Autowired
    private ProviderService providerService;

    public boolean crearProductoDesdeDatos(String datosProducto) {
        // Dividir los datos del producto
        String[] datos = datosProducto.split(",");

        // Verificar si hay suficientes datos para crear el producto
        if (datos.length != 6) {
            System.out.println("Error: Número incorrecto de datos");
            return false;
        }

        // Limpiar la cadena de precio
        String priceCleaned = datos[3].replaceAll("\\D", ""); // Eliminar caracteres no numéricos

        // Crear una instancia de BigInteger con los datos limpiados
        BigInteger price;
        try {
            price = new BigInteger(priceCleaned);
        } catch (NumberFormatException e) {
            System.out.println("Error al crear BigInteger desde la cadena de precio");
            return false;
        }

        // Crear una instancia de Product con los datos recibidos
        Product product = new Product(datos[0], datos[1], Integer.parseInt(datos[2]), price, datos[4], datos[5]);

        // Validar y crear el producto
        boolean productoCreado = validarYCrearProducto(product);
        if (!productoCreado) {
            System.out.println("Error al validar y crear el producto");
        }

        return productoCreado;
    }

    public boolean validarYCrearProducto(Product product) {
        // Otras lógicas de validación...

        // Validar el proveedor utilizando el servicio ProviderService
        if (!providerService.validarProveedor(product.getRutProvider())) {
            System.out.println("Error: Problema al validar el proveedor.");
            return false;
        }

        // Más lógica de validación y creación del producto...

        return true;
    }

// Métodos de validación individuales para cada campo del producto

    private boolean validarCodigoProducto(String code) {
        // Validar código de producto
        boolean isValid = code.matches("[A-Z]\\d+");
        if (!isValid) {
            System.out.println("Error: El código del producto no cumple con el formato requerido.");
        }
        return isValid;
    }

    private boolean validarNombreProducto(String name) {
        // Validar nombre de producto
        boolean isValid = name.length() <= 30;
        if (!isValid) {
            System.out.println("Error: El nombre del producto excede los 30 caracteres permitidos.");
        }
        return isValid;
    }

    private boolean validarStock(String stock) {
        // Validar stock como número entero positivo
        try {
            int stockValue = Integer.parseInt(stock);
            boolean isValid = stockValue >= 0;
            if (!isValid) {
                System.out.println("Error: El stock del producto no es un número válido.");
            }
            return isValid;
        } catch (NumberFormatException e) {
            System.out.println("Error: Problema al convertir el stock a un número entero.");
            return false;
        }
    }






    private boolean validarRutProveedor(String rut) {
        // Validar RUT del proveedor
        boolean isValid = rut.matches("^\\d{1,2}\\.\\d{3}\\.\\d{3}-[0-9kK]{1}$");
        if (!isValid) {
            System.out.println("Error: El RUT del proveedor no cumple con el formato requerido.");
        }
        return isValid;
    }

    private boolean validarPrecio(String price) {
        try {
            // Limpiar caracteres no numéricos ni comas
            String cleanedPrice = price.replaceAll("[^\\d.,]", "");
            // Reemplazar comas por puntos
            cleanedPrice = cleanedPrice.replace(',', '.');
            // Verificar que sea un número válido
            BigInteger priceValue = new BigInteger(cleanedPrice);
            return priceValue.compareTo(BigInteger.ZERO) >= 0;
        } catch (NumberFormatException e) {
            System.out.println("Error: El precio del producto no tiene un formato válido.");
            return false;
        }
    }

    private boolean validarMailProveedor(String mail) {
        // Validar correo del proveedor
        boolean isValid = mail.matches("[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,5}");
        if (!isValid) {
            System.out.println("Error: El correo del proveedor no tiene un formato válido.");
        }
        return isValid;
    }
}
