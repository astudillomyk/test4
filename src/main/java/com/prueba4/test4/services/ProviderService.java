package com.prueba4.test4.services;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

@Service
public class ProviderService {

    private static final String FILE_PATH = "src/main/dataset/proveedores.csv";

    public boolean validarProveedor(String rut) {
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_PATH))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(";");
                if (datos.length >= 2 && datos[0].equals(rut)) {
                    System.out.println("Proveedor encontrado: " + datos[1]);
                    return true; // El RUT del proveedor existe en el archivo CSV
                }
            }
            System.out.println("Proveedor no encontrado para el RUT: " + rut);
        } catch (IOException e) {
            System.out.println("Error: Problema al leer el archivo CSV");
            e.printStackTrace(); // Imprimir detalles del error
        }
        return false; // El RUT del proveedor no existe en el archivo CSV o hubo un error
    }
}

