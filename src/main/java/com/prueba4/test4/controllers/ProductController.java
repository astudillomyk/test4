package com.prueba4.test4.controllers;


import com.prueba4.test4.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/productos")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/crearProducto")
    public ResponseEntity<String> crearProducto(@RequestBody String datosProducto) {
        // Llamar al servicio para validar y crear el producto
        boolean productoCreado = productService.crearProductoDesdeDatos(datosProducto);

        if (productoCreado) {
            return ResponseEntity.ok("Producto creado exitosamente");
        } else {
            return ResponseEntity.badRequest().body("Error al crear el producto");
        }
    }
}
