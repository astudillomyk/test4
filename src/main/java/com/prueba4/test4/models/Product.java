package com.prueba4.test4.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

@Data
@AllArgsConstructor
public class Product {
    private String code;
    private String name;
    private int stock;
    private BigInteger price; // Cambiado a double
    private String rutProvider;
    private String mailProvider;

}

